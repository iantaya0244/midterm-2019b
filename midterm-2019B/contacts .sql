-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2019 at 06:49 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mydatabase`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `phone`) VALUES
(1, 'Gabriel Christian Babiera', 'uzmaknarut024@gmail.com', '02242013'),
(4, 'Ciana Brielle Babiera', 'gabriel.babiera@unc.edu.ph', '01022017'),
(6, 'Crescenda Babiera', 'gi.babiera_24@yahoo.com', '09282156422'),
(7, 'Gabriel Ian Babiera', 'cresgabriel', '09997454485'),
(8, 'gabbyb', 'gabriel.b@gmail.com', '92913910'),
(10, 'gabriel', 'gabriel@gmail.com', '0991293991'),
(11, 'ian', 'gi.babiera_24@yahoo.com', '234234234'),
(12, 'Lebron James', 'lebron.james@gmail.com', '29902141244'),
(13, 'stephen curry', 'curry@gmail.com', '8982340320'),
(14, 'derek rose', 'rose@gmail.com', '09921993129'),
(15, 'kawai leonard', 'leonard@gmail.com', '9982349234'),
(16, 'dwayne wade', 'wade@gmail.com', '00912391231'),
(17, 'Ragnar Lothbrok', 'ragnar@gmail.com', '9912314129'),
(18, 'Manny Pacquaio', 'manny@gmail.com', '991283821'),
(19, 'Miles Flora', 'flora@gmail.com', '09912321345'),
(20, 'Tep Adayo', 'tep@gmail.com', '092174782423');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
